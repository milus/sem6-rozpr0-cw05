To build a project, run:
gradle compileSlice
gradle build
gradle run

Gradle is configured to run as a client. To run as a server, you need to change in build.gradle file mainClassName property to: mainClassName = "com.milus.server.Server".

You can also build a program as a fatjar using "gradle shadowJar"