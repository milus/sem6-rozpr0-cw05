package com.milus.client

import Ice.Communicator
import Ice.FloatHolder
import Ice.IntHolder
import Ice.StringHolder
import Ice.Util
import com.milus.generated.Bank.AccountPrx
import com.milus.generated.Bank.AccountPrxHelper
import com.milus.generated.Bank.BankManagerPrx
import com.milus.generated.Bank.BankManagerPrxHelper
import com.milus.generated.Bank.Currency
import com.milus.generated.Bank.IncorrectAccountNumber
import com.milus.generated.Bank.IncorrectAmount
import com.milus.generated.Bank.IncorrectData
import com.milus.generated.Bank.PersonalData
import com.milus.generated.Bank.PremiumAccountPrx
import com.milus.generated.Bank.PremiumAccountPrxHelper
import com.milus.generated.Bank.RequestRejected
import com.milus.generated.Bank.accountType
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class Client {
    private static final Logger LOGGER = LoggerFactory.getLogger(Client.class)

    private Communicator ic

    private Optional<String> accNumber = Optional.empty()
    private boolean isPremium = false

    public static void main(String[] args) {
        new Client().start()
    }

    def start() {
        try {
            initializeIce()

            def scanner = new Scanner(System.in)

            showCommandMessage()

            readCommandAndDoAction(scanner)

        } catch (Exception e) {
            e.printStackTrace()
        } finally {
            if (ic != null) {
                ic.destroy()
            }
        }
    }

    def readCommandAndDoAction(Scanner scanner) {
        while (scanner.hasNext()) {
            def command = scanner.next()
            switch (command) {
                case "c":
                    !accNumber.isPresent() ? createAccount(scanner) : rejectCommand()
                    break
                case "n":
                    accNumber.isPresent() ? getAccountNumber() : rejectCommand()
                    break
                case "b":
                    accNumber.isPresent() ? getCurrentBalance() : rejectCommand()
                    break
                case "r":
                    accNumber.isPresent() ? removeAccount() : rejectCommand()
                    break
                case "t":
                    accNumber.isPresent() ? transferMoney(scanner) : rejectCommand()
                    break
                case "l":
                    (accNumber.isPresent() && isPremium) ? takeLoan(scanner) : rejectCommand()
                    break
                case "x":
                    println("wanted to exit, bye")
                    return
                default:
                    rejectCommand()
                    break
            }
            showCommandMessage()
        }
    }

    def initializeIce() {
        String[] config = ["--Ice.Config=config.client"]
        ic = Util.initialize(config)
        LOGGER.info("Communicator initialized")
    }

    def showCommandMessage() {
        println("Please type in a command. Allowed:")
        if (accNumber.isPresent()) {
            println("n - gets account number")
            println("b - gets current balance")
            println("r - removes an account")
            println("t - transfer amount to your account")
            if (isPremium) {
                println "l - takes loan only if you have premium account"
            }
        } else {
            println("c - creates an account")
        }

        println("x - exits from program")
    }

    BankManagerPrx getBankManager() {
        def proxy = ic.stringToProxy("common/manager:tcp -h 127.0.0.1 -p 10000")
        return BankManagerPrxHelper.checkedCast(proxy)
    }

    AccountPrx getAccount() {
        def proxy = ic.stringToProxy("acc/${accNumber.get()}:tcp -h 127.0.0.1 -p 10000")
        return AccountPrxHelper.checkedCast(proxy)
    }

    PremiumAccountPrx getPremiumAccount() {
        def proxy = ic.stringToProxy("acc/${accNumber.get()}:tcp -h 127.0.0.1 -p 10000")
        return PremiumAccountPrxHelper.checkedCast(proxy)
    }

    def createAccount(Scanner scanner) {
        def firstName
        def lastName
        def nationality
        def natID
        def accType

        println("Name:")
        if (!scanner.hasNext()) {
            return
        }
        firstName = scanner.next()

        println("last name")
        if (!scanner.hasNext()) {
            return
        }
        lastName = scanner.next()

        println("nationality")
        if (!scanner.hasNext()) {
            return
        }
        nationality = scanner.next()

        println("nationalID")
        if (!scanner.hasNext()) {
            return
        }
        natID = scanner.next()

        println("Account type")
        println "S - silver"
        println "P - premium"
        if (!scanner.hasNext()) {
            return
        }
        def response = scanner.next()
        switch (response) {
            case "P":
                accType = accountType.PREMIUM
                break
            default:
                accType = accountType.SILVER
                break
        }


        def personalData = new PersonalData(firstName, lastName, nationality, natID)
        def holder = new StringHolder()
        try {
            getBankManager().createAccount(personalData, accType, holder)
            accNumber = Optional.of(holder.value)
            if (accType == accountType.PREMIUM) {
                isPremium = true
            }
            println("Account is added. Its number is " + accNumber)
        } catch (IncorrectData ignored) {
            println "Your data is incorrect."
        } catch (RequestRejected ignored) {
            println "I am so sorry. Your request has been rejected. Maybe you are a criminal..."
        }
    }

    def removeAccount() {
        getBankManager().removeAccount(accNumber.get())
    }

    def getAccountNumber() {
        def proxy
        if (isPremium) {
            proxy = getPremiumAccount()
        } else {
            proxy = getAccount()
        }
        println proxy.getAccountNumber()
    }

    def getCurrentBalance() {
        def proxy
        if (isPremium) {
            proxy = getPremiumAccount()
        } else {
            proxy = getAccount()
        }
        println proxy.getBalance()
    }

    def transferMoney(Scanner scanner) {
        def accNumber
        def amount

        println "To which number you want to transfer your money?"
        if (!scanner.hasNext()) {
            return
        }
        accNumber = scanner.next()

        println "How much?"
        if (!scanner.hasNextInt()) {
            return
        }
        amount = scanner.nextInt()

        try {
            def proxy
            if (isPremium) {
                proxy = getPremiumAccount()
            } else {
                proxy = getAccount()
            }
            proxy.transferMoney(accNumber, amount)
            println "Transfered!"
        } catch (IncorrectAccountNumber ignored) {
            println "Incorrect account number: ${accNumber}"
        } catch (IncorrectAmount ignored) {
            println "Incorrect amount"
        }
    }

    def takeLoan(Scanner scanner) {
        def amount
        def curr
        def period
        def interestRate = new FloatHolder()
        def totalCost = new IntHolder()

        println "How much?"
        if (!scanner.hasNextInt()) {
            return
        }
        amount = scanner.nextInt()

        println "In which currency?"
        println "Possibilities:"
        println "U - USD"
        println "C - CHF"
        println "E - EUR"
        println "P - PLN"
        if (!scanner.hasNext()) {
            return
        }
        def response = scanner.next()
        switch (response) {
            case "U":
                curr = Currency.USD
                break
            case "C":
                curr = Currency.CHF
                break
            case "E":
                curr = Currency.EUR
                break
            default:
                curr = Currency.PLN
                break
        }

        println "For how long?"
        if (!scanner.hasNextInt()) {
            return
        }
        period = scanner.nextInt()

        getPremiumAccount().calculateLoan(amount, curr, period, interestRate, totalCost)

        println "Current interest rate: ${interestRate.value}"
        println "Total cost: ${totalCost.value}"
    }

    static def rejectCommand() {
        println "This command is not allowed. Please choose another one."
    }
}
