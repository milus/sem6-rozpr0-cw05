package com.milus.server.ice.servant;

import Ice.Current;
import Ice.FloatHolder;
import Ice.IntHolder;
import com.milus.generated.Bank.*;
import com.milus.server.domain.Account;
import com.milus.server.domain.AccountHolder;
import com.milus.server.domain.NoSuchAccountException;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RequiredArgsConstructor(staticName = "create")
public class PremiumAccountServant extends _PremiumAccountDisp {

    private static final Logger LOGGER = LoggerFactory.getLogger(PremiumAccountServant.class);

    private final AccountHolder accountHolder = AccountHolder.getInstance();

    private final Account account;

    @Override
    public int getBalance(Current __current) {
        LOGGER.info("getBalance for: " + account);
        return account.getBalance();
    }

    @Override
    public String getAccountNumber(Current __current) {
        LOGGER.info("getAccountNumber for: " + account);
        return account.getNumber();
    }

    @Override
    public void transferMoney(String accountNumber, int amount, Current __current) throws IncorrectAccountNumber, IncorrectAmount {
        try {
            account.transfer(amount).to(accountNumber);
            LOGGER.info("transferred " + amount + " to " + accountNumber);
        } catch (NoSuchAccountException e) {
            throw new IncorrectAccountNumber();
        }
    }

    @Override
    public void calculateLoan(int amount, Currency curr, int period, FloatHolder interestRateHolder, IntHolder totalCostHolder, Current __current) throws IncorrectData {
        float interestRate = 0.1f;

        account.takeLoan(amount, curr, period, interestRate);

        interestRateHolder.value = interestRate;
        totalCostHolder.value = (int) (((period / 12.0) * interestRate) * amount);
    }
}
