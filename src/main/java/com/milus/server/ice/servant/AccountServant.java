package com.milus.server.ice.servant;

import Ice.Current;
import com.milus.generated.Bank.IncorrectAccountNumber;
import com.milus.generated.Bank.IncorrectAmount;
import com.milus.generated.Bank._AccountDisp;
import com.milus.server.domain.Account;
import com.milus.server.domain.AccountHolder;
import com.milus.server.domain.NoSuchAccountException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RequiredArgsConstructor(staticName = "create")
public class AccountServant extends _AccountDisp {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountServant.class);

    private final AccountHolder accountHolder = AccountHolder.getInstance();

    @Getter
    private final Account account;

    @Override
    public int getBalance(Current __current) {
        LOGGER.info("getBalance for: " + account);
        return account.getBalance();
    }

    @Override
    public String getAccountNumber(Current __current) {
        LOGGER.info("getAccountNumber for: " + account);
        return account.getNumber();
    }

    @Override
    public void transferMoney(String accountNumber, int amount, Current __current) throws IncorrectAccountNumber, IncorrectAmount {
        try {
            account.transfer(amount).to(accountNumber);
            LOGGER.info("transferred " + amount + " to " + accountNumber);
        } catch (NoSuchAccountException e) {
            throw new IncorrectAccountNumber();
        }
    }

}
