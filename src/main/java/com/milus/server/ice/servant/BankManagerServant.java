package com.milus.server.ice.servant;

import Ice.Current;
import Ice.Identity;
import Ice.StringHolder;
import com.milus.generated.Bank.*;
import com.milus.server.Server;
import com.milus.server.domain.Account;
import com.milus.server.domain.AccountHolder;
import com.milus.server.domain.BankManager;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RequiredArgsConstructor(staticName = "create")
public class BankManagerServant extends _BankManagerDisp {
    private static final Logger LOGGER = LoggerFactory.getLogger(BankManagerServant.class);

    private final AccountHolder accountHolder = AccountHolder.getInstance();

    private final BankManager bankManager;

    @Override
    public void createAccount(PersonalData data, accountType type, StringHolder accountID, Current current)
            throws IncorrectData, RequestRejected {
        if (!isPersonalDataOk(data)) {
            throw new IncorrectData();
        }

        Account.Type accountType = Account.Type.fromRemote(type);

        String createdAccount = bankManager.createAccount(accountType);

        // ICE horror
        if (accountType == Account.Type.PREMIUM) {
            current.adapter.add(
                    PremiumAccountServant.create(accountHolder.loadToMemory(createdAccount).get()),
                    new Identity(createdAccount, Server.ACCOUNT_CATEGORY));
        }
        accountID.value = createdAccount;

        LOGGER.info("Added account with number: " + createdAccount);
    }

    @Override
    public void removeAccount(String accountID, Current __current) throws IncorrectData, NoSuchAccount {
        if (StringUtils.isBlank(accountID)) {
            throw new IncorrectData();
        }

        if (bankManager.contains(accountID)) {
            bankManager.removeAccount(accountID);
        } else {
            throw new NoSuchAccount();
        }

    }

    private boolean isPersonalDataOk(PersonalData data) {
        if (StringUtils.isBlank(data.firstName)) {
            return false;
        }

        if (StringUtils.isBlank(data.lastName)) {
            return false;
        }

        if (StringUtils.isBlank(data.nationality)) {
            return false;
        }

        if (StringUtils.isBlank(data.nationalIDNumber)) {
            return false;
        }

        return true;
    }
}
