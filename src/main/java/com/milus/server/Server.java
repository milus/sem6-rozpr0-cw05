package com.milus.server;

import Ice.ObjectAdapter;
import Ice.ServantLocator;
import com.milus.server.domain.BankManager;
import com.milus.server.ice.servant.BankManagerServant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Server {
    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);

    public static final String ACCOUNT_CATEGORY = "acc";

    public static void main(String[] args) {
        Ice.Communicator ic = null;
        try {
            String[] config = new String[]{"--Ice.Config=config.server"};

            ic = Ice.Util.initialize(config);
            LOGGER.info("Communicator initialized");

            ObjectAdapter adapter = ic.createObjectAdapter("adapter");

            ServantLocator evictor = new SilverAccountServantEvictor(1);

            adapter.addServantLocator(evictor, ACCOUNT_CATEGORY);
            LOGGER.info("Evictor added " + evictor);

            BankManager bankManager = new BankManager();
            BankManagerServant bankManagerServant = BankManagerServant.create(bankManager);

            adapter.addDefaultServant(bankManagerServant, "common");

            adapter.activate();
            LOGGER.info("Adapter has been activated");

            LOGGER.info("Server started");
            ic.waitForShutdown();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ic != null) {
                ic.destroy();
            }
        }
    }

}
