package com.milus.server.domain;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.milus.generated.Bank.*;
import com.milus.generated.Bank.Currency;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class AccountHolder {
    private final static AccountHolder instance = new AccountHolder();

    private static final String DATA_DIR = "data/";

    private final Gson gson = new Gson();

    private final Map<String, Account.Type> registered = Maps.newConcurrentMap();
    private final Map<String, Account> cache = Maps.newConcurrentMap();

    public static AccountHolder getInstance() {
        return instance;
    }

    public void saveToFile(@NonNull Account account) {
        try (
                PrintWriter pw = new PrintWriter(DATA_DIR + account.getNumber())
        ) {
            pw.print(gson.toJson(account));
            cache.remove(account.getNumber());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Optional<Account> loadToMemory(@NonNull String accountNumber) {
        Optional<Account> account = loadToMemoryWithoutCaching(accountNumber);
        account.ifPresent(a -> cache.put(accountNumber, a));
        return account;
    }

    Optional<Account> get(@NonNull String accountNumber) {
        if (!registered.containsKey(accountNumber)) {
            return Optional.empty();
        }

        if (cache.containsKey(accountNumber)) {
            return Optional.of(cache.get(accountNumber));
        }

        return loadToMemoryWithoutCaching(accountNumber).map((account) -> {
            if (account instanceof SilverAccount) {
                return new TransferTargetAccount((SilverAccount) account);
            }
            return account;
        });
    }

    Set<String> getRegistered() {
        return Collections.unmodifiableSet(registered.keySet());
    }

    void register(@NonNull String id, Account.Type type) {
        registered.put(id, type);
    }

    void unregister(@NonNull String id) {
        registered.remove(id);
    }

    boolean contains(@NonNull String accountID) {
        return registered.keySet().stream()
                .anyMatch(accountNumber -> accountNumber.equals(accountID));
    }

    private Optional<Account> loadToMemoryWithoutCaching(@NonNull String accountNumber) {
        if (!registered.containsKey(accountNumber)) {
            return Optional.empty();
        }

        if (cache.containsKey(accountNumber)) {
            return Optional.of(cache.get(accountNumber));
        }
        Optional<Account> loaded = tryToLoadFromFile(accountNumber);
        if (loaded.isPresent()) {
            return loaded;
        } else {
            return Optional.of(registered.get(accountNumber).createAccount(accountNumber));
        }
    }

    private Optional<Account> tryToLoadFromFile(@NonNull String accountNumber) {
        try {
            SilverAccount account = gson.fromJson(new Scanner(new File(DATA_DIR + accountNumber)).useDelimiter("\\Z").next(), SilverAccount.class);
            return Optional.of(account);
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    /**
     * This class is not intended to normal use. It's only purpose is be a receiver of transfer!
     */
    @RequiredArgsConstructor
    private static class TransferTargetAccount implements Account {

        private final SilverAccount account;
        private boolean used = false;

        @Override
        public int getBalance() {
            throw new UnsupportedOperationException();
        }

        @Override
        public String getNumber() {
            throw new UnsupportedOperationException();
        }

        @Override
        public TransferBuilder transfer(int amount) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void increase(int amount) {
            if (used) {
                throw new UnsupportedOperationException("Object has been already used");
            }
            account.increase(amount);
            used = true;
            saveToFile();
        }

        @Override
        public void decrease(int amount) {
            if (used) {
                throw new UnsupportedOperationException("Object has been already used");
            }
            account.decrease(amount);
            used = true;
            saveToFile();
        }

        @Override
        public void takeLoan(int amount, Currency curr, int period, float interestRate) {
            throw new UnsupportedOperationException();
        }

        private void saveToFile() {
            try (
                    PrintWriter pw = new PrintWriter(DATA_DIR + account.getNumber())
            ) {
                Gson gson = new Gson();
                pw.print(gson.toJson(account));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
