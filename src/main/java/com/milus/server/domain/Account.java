package com.milus.server.domain;

import com.milus.generated.Bank.Currency;
import com.milus.generated.Bank.accountType;

public interface Account {
    int getBalance();

    String getNumber();

    TransferBuilder transfer(int amount);

    void increase(int amount);

    void decrease(int amount);

    void takeLoan(int amount, Currency curr, int period, float interestRate);

    interface TransferBuilder {
        void to(String accountNumber) throws NoSuchAccountException;
    }

    enum Type {
        SILVER {
            @Override
            public Account createAccount(String name) {
                return SilverAccount.create(name);
            }
        }, PREMIUM {
            @Override
            public Account createAccount(String name) {
                return PremiumAccount.create(name);
            }
        };
        
        public abstract Account createAccount(String name);

        public static Type fromRemote(accountType type) {
            switch (type) {
                case PREMIUM:
                    return PREMIUM;
                case SILVER:
                    return SILVER;
                default:
                    return SILVER;
            }
        }
    }
}
