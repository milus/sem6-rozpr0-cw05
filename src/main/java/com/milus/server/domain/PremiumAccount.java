package com.milus.server.domain;

import com.milus.generated.Bank.Currency;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor(staticName = "create")
@EqualsAndHashCode(of = "number")
@ToString
public class PremiumAccount implements Account {

    private final String number;
    private int balance;

    @Override
    public String getNumber() {
        return number;
    }

    @Override
    public int getBalance() {
        return balance;
    }

    @Override
    public TransferBuilder transfer(int amount) {
        return new PremiumTransferBuilder(amount);
    }

    @Override
    public void increase(int amount) {
        balance += amount;
    }

    @Override
    public void decrease(int amount) {
        balance -= amount;
    }

    @Override
    public void takeLoan(int amount, Currency curr, int period, float interestRate) {
        balance += amount;
    }

    @RequiredArgsConstructor
    private class PremiumTransferBuilder implements TransferBuilder {

        private final int amount;
        private final AccountHolder accountHolder = AccountHolder.getInstance();

        @Override
        public void to(String accountNumber) throws NoSuchAccountException {
            if (!accountHolder.contains(accountNumber)) {
                throw new NoSuchAccountException();
            }
            decrease(amount);
            accountHolder.get(accountNumber).get().increase(amount);
        }
    }
}
