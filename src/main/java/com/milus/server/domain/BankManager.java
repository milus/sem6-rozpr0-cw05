package com.milus.server.domain;

public class BankManager {

    private final AccountHolder holder = AccountHolder.getInstance();

    public String createAccount(Account.Type type) {
        String accountNumber = resolveNewAccountNumber();

        holder.register(accountNumber, type);

        return accountNumber;
    }

    public boolean contains(String accountID) {
        return holder.contains(accountID);
    }

    public void removeAccount(String accountID) {
        holder.unregister(accountID);
    }

    private String resolveNewAccountNumber() {
        int[] numbers = holder.getRegistered().stream()
                .mapToInt(Integer::parseInt)
                .sorted()
                .toArray();

        if (numbers.length == 0) {
            return String.valueOf(0);
        }

        if (numbers.length == 1) {
            return String.valueOf(numbers[0] + 1);
        }

        int lowest = numbers[0];
        for (int number : numbers) {
            if (lowest + 1 < number) {
                return String.valueOf(lowest + 1);
            }
            lowest = number;
        }
        return String.valueOf(lowest + 1);
    }
}
