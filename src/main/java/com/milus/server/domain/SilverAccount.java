package com.milus.server.domain;

import com.milus.generated.Bank.Currency;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.Optional;

@RequiredArgsConstructor(staticName = "create")
@EqualsAndHashCode(of = "number")
@ToString
public class SilverAccount implements Account {

    private final String number;
    private int balance;

    @Override
    public String getNumber() {
        return number;
    }

    @Override
    public int getBalance() {
        return balance;
    }

    @Override
    public TransferBuilder transfer(int amount) {
        return new SilverTransferBuilder(amount);
    }

    @Override
    public void increase(int amount) {
        balance += amount;
    }

    @Override
    public void decrease(int amount) {
        balance -= amount;
    }

    @Override
    public void takeLoan(int amount, Currency curr, int period, float interestRate) {
        throw new UnsupportedOperationException();
    }

    @RequiredArgsConstructor
    private class SilverTransferBuilder implements TransferBuilder {

        private final int amount;
        private final AccountHolder accountHolder = AccountHolder.getInstance();

        @Override
        public void to(String accountNumber) throws NoSuchAccountException {
            if (!accountHolder.contains(accountNumber)) {
                throw new NoSuchAccountException();
            }
            decrease(amount);
            accountHolder.get(accountNumber).get().increase(amount);
        }
    }
}
