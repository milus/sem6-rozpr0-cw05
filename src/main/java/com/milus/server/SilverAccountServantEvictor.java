package com.milus.server;

import Ice.*;
import Ice.Object;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import com.milus.server.domain.Account;
import com.milus.server.domain.AccountHolder;
import com.milus.server.ice.servant.AccountServant;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;
import java.util.Queue;

@RequiredArgsConstructor
public class SilverAccountServantEvictor implements ServantLocator {
    private static final Logger LOGGER = LoggerFactory.getLogger(SilverAccountServantEvictor.class);

    private final AccountHolder accountHolder = AccountHolder.getInstance();

    private final int limit;

    private final Map<String, AccountServant> servants = Maps.newConcurrentMap();
    private final Queue<String> lastAccessedAccounts = Queues.newConcurrentLinkedQueue();

    @Override
    public Ice.Object locate(Current current, LocalObjectHolder localObjectHolder) throws UserException {
        LOGGER.info("id: " + current.id.name);

        if (servants.containsKey(current.id.name)) {
            return servants.get(current.id.name);
        } else if (lastAccessedAccounts.size() < limit) {
            return loadServant(current.id.name);
        } else {
            storeLastServant();
            return loadServant(current.id.name);
        }
    }

    @Override
    public void deactivate(String s) {

    }

    @Override
    public void finished(Current current, Object object, java.lang.Object o) throws UserException {

    }

    private void storeLastServant() {
        String lastAccessedId = lastAccessedAccounts.poll();
        AccountServant lastAccessedServant = servants.get(lastAccessedId);

        accountHolder.saveToFile(lastAccessedServant.getAccount());
        servants.remove(lastAccessedId);
    }

    private Optional<Account> loadAccount(String id) {
        return accountHolder.loadToMemory(id);
    }

    private AccountServant loadServant(String id) {
        Optional<Account> account = loadAccount(id);
        if (account.isPresent()) {
            AccountServant servant = AccountServant.create(account.get());
            servants.put(id, servant);
            lastAccessedAccounts.add(id);
            return servant;
        }
        return null;
    }
}
